@extends('layout.main')

@section('meta')
    <title>ROT Home</title>
    <!-- start: META -->
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!--<link rel="canonical" href=""/>-->
    <meta name="robots" content="index,follow"/>
    <!-- facebook like snippet -->
    <meta property="og:locale" content=""/>
    <meta property="og:tittle" content=""/>
    <meta property="og:description" content=""/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content=""/>
    <meta property="og:type" content="website"/>
    <!-- end: facebook like snippet -->
    <!-- end: META -->
@stop
@section('styles')
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
@stop
@section('mainFeature')

@stop
@section('content')
    <div id="index">
        <section class="container-fluid">
            <div class="container main">
                <div class="col-xs-12 main-img">
                    <img src="{{asset('images/rotband.jpg')}}" class="img-responsive">
                </div>
                <div class="col-xs-12 gigs">
                    <h1>GIGS</h1>
                    <hr>
                    <div class="col-xs-12 col-sm-4 main-txt">
                        <h4>Charcaslovakia 2017</h4>
                        <div class="col-xs-12">

                            <p>Cuauhtemoc #108</p>
                            <p>Charcas, S.L.P.</p>
                            <p><span>Rockeado</span></p>
                        </div>
                        <div class="col-xs-12">
                            <a href="https://goo.gl/maps/MQwgXz6GRt12"><i class="fa fa-map-marker" aria-hidden="true"></i></a>
                            <a href="https://www.facebook.com/hermandadelmetal/"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                            <!--<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>-->
                        </div>
                    </div>
                    <div class=" col-xs-12 col-sm-4 main-txt">
                        <h4>Mexican Deathgeneration</h4>
                        <div class="col-xs-12 ">

                            <p>Bunker Sala de Conciertos</p>
                            <p>Reforma #1106. Z.C.</p>
                            <p><span>16/03/2018 19:00</span></p>
                        </div>
                        <div class="col-xs-12">
                            <a href="https://goo.gl/maps/81vkPdkBSas"><i class="fa fa-map-marker" aria-hidden="true"></i></a>
                            <a href="https://www.facebook.com/Bunker-Sala-de-Conciertos-640268719354000/"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                            <!--<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>-->
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 main-txt">
                        <h4>Vaginal Anomalies</h4>
                        <div class="col-xs-12 ">

                            <p>LOUD Open Stage</p>
                            <p>Agustín de Iturbide #640 Z.C.</p>
                            <p><span>09/01/2018 20:00</span></p>
                        </div>
                        <div class="col-xs-12">
                            <a href="https://goo.gl/maps/t2Hg9mN8rHC2"><i class="fa fa-map-marker" aria-hidden="true"></i></a>
                            <a href="https://www.facebook.com/loud.openstage/"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                            <!--<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>-->
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <section class="container-fluid content">
            <div class="container">

                    <div class="col-xs-12 col-md-6">
                        <img src="{{asset('images/rotband-crew.jpg')}}" class="img-responsive">
                        <p>Somos una banda potosina enfocada en el género Brutal Death Metal, acualmente estamos enfocados en nuestro nuevo disco lleno de podredumbre y poder. Te invitamos a seguirnos y apoyarnos para un buen rato de metal podrido.</p>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="news">
                            <h2>Rot News</h2>
                            <hr>
                            <h4>Mexican Deathgeneration Tour</h4>
                            <p>Atentos vamos a formar parte de este evento en <a href="https://www.facebook.com/Bunker-Sala-de-Conciertos-640268719354000/">Bunker Sala de concietos</a> el Viernes 16 de Marzo de 2018, Tocarán desde España <a href="https://www.facebook.com/1991Avulsed/">AVULSED</a> no se la pierdan, desde Ecuador PARCA y buenas bandas locales como <a href="https://www.facebook.com/stenchofrottengore/">Stench of rotten gore</a>, <a href="https://www.facebook.com/ForgingMetalBand/">Forging Metal</a> y Miserable vengan listos a calentar garganta con cheves y metal podrido.</p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <iframe src="https://embed.spotify.com/?uri=spotify%3Auser%3Asidknot%3Aplaylist%3A7k2W01dTxkEKpscBQ2tekR" width="260" height="380" frameborder="0" allowtransparency="true"></iframe>
                    </div>
            </div>
        </section>
        <section class="container-fluid promo">
            <div class="container">
                <img src="{{asset('images/rot-sales.jpg')}}" class="img-responsive">
            </div>
        </section>









    </div>
@stop
@section('scripts')
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
@stop