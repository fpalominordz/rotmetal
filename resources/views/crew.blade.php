@extends('layout.main')

@section('meta')
    <title>ROT Crew</title>
    <!-- start: META -->
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!--<link rel="canonical" href=""/>-->
    <meta name="robots" content="index,follow"/>
    <!-- facebook like snippet -->
    <meta property="og:locale" content=""/>
    <meta property="og:tittle" content=""/>
    <meta property="og:description" content=""/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content=""/>
    <meta property="og:type" content="website"/>
    <!-- end: facebook like snippet -->
    <!-- end: META -->
@stop
@section('styles')
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
@stop
@section('mainFeature')

@stop
@section('content')
    <div id="crew">
        <section class="container-fluid">
            <div class="container main">
                <div class="col-sm-3 bio">
                    <img src="{{asset('images/richi-war.jpg')}}" class="img-responsive">
                    <h3>Ricardo Guerra</h3>
                    <h4>Guitarra principal</h4>
                    <p></p>
                </div>
                <div class="col-sm-3 bio">
                    <img src="{{asset('images/yisus.jpg')}}" class="img-responsive">
                    <h3>Rodrigo Guerra</h3>
                    <h4>Batería</h4>
                    <p></p>
                </div>
                <div class="col-sm-3 bio">
                    <img src="{{asset('images/oso-babas.jpg')}}" class="img-responsive">
                    <h3>Oscar Sánchez</h3>
                    <h4>Bajo</h4>
                    <p></p>
                </div>
                <div class="col-sm-3 bio">
                    <img src="{{asset('images/pato-brujote.jpg')}}" class="img-responsive">
                    <h3>Alfredo Valero</h3>
                    <h4>Vocal</h4>
                    <p></p>
                </div>
            </div>
        </section>

        <section class="container-fluid promo">
            <div class="container">
                <img src="{{asset('images/rot-sales.jpg')}}" class="img-responsive">
            </div>
        </section>









    </div>
@stop
@section('scripts')
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
@stop