<header class="container-fluid">
    <div class="container menu">
        <div id="main-menu" class="navbar navbar-default " role="navigation">
            <div class="container-fluid">
                <div class="navbar-header"><a class="logo col-xs-5 col-sm-2 col-md-5 col-lg-5" href="{{route('index')}}"><img src="{{asset('images/rot-logo-200px.png')}}" class="img-responsive center-block"> </a>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-menubuilder"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse navbar-menubuilder">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="{{route('crew')}}">Crew</a></li>
                        <!--<li><a href="#">Música</a></li>
                        <li><a href="#">Stuff</a></li>-->
                        <li><a href="{{route('contact')}}">Contacto</a></li>
                        <li><a href="https://www.facebook.com/ROTMETAL/"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
</header>