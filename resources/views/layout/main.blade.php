<!DOCTYPE html>
<!--[if IE 8]><html class="ie8" lang="es"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="es"><![endif]-->
<!--[if !IE]><!-->
<html lang="es">
<!--<![endif]-->
<!-- start: HEAD -->
<head>
    @yield('meta')
        <!--<link rel="shortcut icon" href="{{asset('favicon.ico')}}"/>-->
        <!-- start: MAIN CSS-->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Metal+Mania|Open+Sans:300,400,700&subset=latin-ext" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('css/style.css')}}">
        <!-- end: MAIN CSS -->
        <!-- google console verification-->

        <!-- font awesome -->
        <script src="https://use.fontawesome.com/97fe9dbd08.js"></script>
    @yield('styles')
</head>
<!-- end: HEAD -->
<body>

@include('partials.header')
@yield('mainFeature')
@yield('content')
@include('partials.footer')

<!-- start: MAIN JAVASCRIPTS -->
<!-- google jquery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<!-- bootstrap js -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<!-- google analytics -->
<!-- end: MAIN JAVASCRIPTS -->
@yield('scripts')

</body>